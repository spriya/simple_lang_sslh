	.text
	.file	"lib.c"
	.globl	simple                          # -- Begin function simple
	.p2align	4, 0x90
	.type	simple,@function
simple:                                 # @simple
.Lfunc_begin0:
	.file	1 "/home/basavesh/research/simple_lang_sslh/LLVM" "lib.c"
	.loc	1 1 0                           # lib.c:1:0
	.cfi_sections .debug_frame
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	$-1, %rax
	movq	%rsp, %r8
	sarq	$63, %r8
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movq	%rdx, -16(%rbp)
	movl	%ecx, -20(%rbp)
.Ltmp0:
	.loc	1 2 18 prologue_end             # lib.c:2:18
	movl	-20(%rbp), %ecx
	.loc	1 2 9 is_stmt 0                 # lib.c:2:9
	movl	%ecx, -24(%rbp)
.Ltmp1:
	.loc	1 3 9 is_stmt 1                 # lib.c:3:9
	movl	-4(%rbp), %ecx
	.loc	1 3 11 is_stmt 0                # lib.c:3:11
	cmpl	-8(%rbp), %ecx
	movq	%rax, -32(%rbp)                 # 8-byte Spill
	movq	%r8, -40(%rbp)                  # 8-byte Spill
.Ltmp2:
	.loc	1 3 9                           # lib.c:3:9
	jle	.LBB0_2
# %bb.1:
	.loc	1 0 9                           # lib.c:0:9
	movq	-40(%rbp), %rax                 # 8-byte Reload
	movq	-32(%rbp), %rcx                 # 8-byte Reload
	cmovleq	%rcx, %rax
.Ltmp3:
	.loc	1 4 19 is_stmt 1                # lib.c:4:19
	movl	-4(%rbp), %edx
	.loc	1 4 16 is_stmt 0                # lib.c:4:16
	addl	-24(%rbp), %edx
	movl	%edx, -24(%rbp)
	movq	%rax, -48(%rbp)                 # 8-byte Spill
	.loc	1 5 5 is_stmt 1                 # lib.c:5:5
	jmp	.LBB0_3
.Ltmp4:
.LBB0_2:
	.loc	1 0 5 is_stmt 0                 # lib.c:0:5
	movq	-40(%rbp), %rax                 # 8-byte Reload
	movq	-32(%rbp), %rcx                 # 8-byte Reload
	cmovgq	%rcx, %rax
.Ltmp5:
	.loc	1 6 19 is_stmt 1                # lib.c:6:19
	movl	-8(%rbp), %edx
	.loc	1 6 16 is_stmt 0                # lib.c:6:16
	addl	-24(%rbp), %edx
	movl	%edx, -24(%rbp)
	movq	%rax, -48(%rbp)                 # 8-byte Spill
.Ltmp6:
.LBB0_3:
	.loc	1 0 16                          # lib.c:0:16
	movq	-48(%rbp), %rax                 # 8-byte Reload
	.loc	1 8 10 is_stmt 1                # lib.c:8:10
	movl	-24(%rbp), %ecx
	.loc	1 8 6 is_stmt 0                 # lib.c:8:6
	movq	-16(%rbp), %rdx
	.loc	1 8 8                           # lib.c:8:8
	movl	%ecx, (%rdx)
	.loc	1 9 1 is_stmt 1                 # lib.c:9:1
	shlq	$47, %rax
	orq	%rax, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Ltmp7:
.Lfunc_end0:
	.size	simple, .Lfunc_end0-simple
	.cfi_endproc
                                        # -- End function
	.globl	array                           # -- Begin function array
	.p2align	4, 0x90
	.type	array,@function
array:                                  # @array
.Lfunc_begin1:
	.loc	1 11 0                          # lib.c:11:0
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$96, %rsp
	movq	$-1, %rax
	movq	%rsp, %r8
	sarq	$63, %r8
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movl	%edx, -20(%rbp)
	movq	%rcx, -32(%rbp)
.Ltmp8:
	.loc	1 13 11 prologue_end            # lib.c:13:11
	cmpl	$10, -20(%rbp)
	movq	%rax, -48(%rbp)                 # 8-byte Spill
	movq	%r8, -56(%rbp)                  # 8-byte Spill
.Ltmp9:
	.loc	1 13 9 is_stmt 0                # lib.c:13:9
	jle	.LBB2_2
# %bb.1:
	.loc	1 0 9                           # lib.c:0:9
	movq	-56(%rbp), %rax                 # 8-byte Reload
	movq	-48(%rbp), %rcx                 # 8-byte Reload
	cmovleq	%rcx, %rax
.Ltmp10:
	.loc	1 14 13 is_stmt 1               # lib.c:14:13
	movq	-8(%rbp), %rdx
	movl	(%rdx), %esi
	movl	%eax, %edi
	orl	%esi, %edi
	.loc	1 14 11 is_stmt 0               # lib.c:14:11
	movl	%edi, -20(%rbp)
	.loc	1 15 13 is_stmt 1               # lib.c:15:13
	movq	-8(%rbp), %rdx
	movl	(%rdx), %esi
	movl	%eax, %edi
	orl	%esi, %edi
	.loc	1 15 11 is_stmt 0               # lib.c:15:11
	movl	%edi, -36(%rbp)
	.loc	1 16 19 is_stmt 1               # lib.c:16:19
	movl	-36(%rbp), %esi
	.loc	1 16 9 is_stmt 0                # lib.c:16:9
	movq	-8(%rbp), %rdx
	.loc	1 16 17                         # lib.c:16:17
	movl	%esi, 20(%rdx)
	.loc	1 17 9 is_stmt 1                # lib.c:17:9
	movq	-8(%rbp), %rdx
	.loc	1 17 17 is_stmt 0               # lib.c:17:17
	movl	16(%rdx), %esi
	movl	%eax, %edi
	orl	%esi, %edi
	addl	$5, %edi
	movl	%edi, 16(%rdx)
	.loc	1 18 13 is_stmt 1               # lib.c:18:13
	movq	-16(%rbp), %rdx
	movl	8(%rdx), %esi
	movl	%eax, %edi
	orl	%esi, %edi
	.loc	1 18 11 is_stmt 0               # lib.c:18:11
	movl	%edi, -40(%rbp)
	.loc	1 19 19 is_stmt 1               # lib.c:19:19
	movl	-40(%rbp), %esi
	.loc	1 19 9 is_stmt 0                # lib.c:19:9
	movq	-16(%rbp), %rdx
	.loc	1 19 17                         # lib.c:19:17
	movl	%esi, 28(%rdx)
	.loc	1 20 9 is_stmt 1                # lib.c:20:9
	movq	-8(%rbp), %rdx
	.loc	1 20 17 is_stmt 0               # lib.c:20:17
	movl	12(%rdx), %esi
	movl	%eax, %edi
	orl	%esi, %edi
	addl	$291, %edi                      # imm = 0x123
	movl	%edi, 12(%rdx)
	movq	%rax, -64(%rbp)                 # 8-byte Spill
	.loc	1 21 5 is_stmt 1                # lib.c:21:5
	jmp	.LBB2_3
.Ltmp11:
.LBB2_2:
	.loc	1 0 5 is_stmt 0                 # lib.c:0:5
	movq	-56(%rbp), %rax                 # 8-byte Reload
	movq	-48(%rbp), %rcx                 # 8-byte Reload
	cmovgq	%rcx, %rax
.Ltmp12:
	.loc	1 22 13 is_stmt 1               # lib.c:22:13
	movq	-8(%rbp), %rdx
	movl	(%rdx), %esi
	movl	%eax, %edi
	orl	%esi, %edi
	.loc	1 22 11 is_stmt 0               # lib.c:22:11
	movl	%edi, -36(%rbp)
	.loc	1 23 13 is_stmt 1               # lib.c:23:13
	movq	-8(%rbp), %rdx
	movslq	-36(%rbp), %r8
	movl	(%rdx,%r8,4), %esi
	movl	%eax, %edi
	orl	%esi, %edi
	.loc	1 23 11 is_stmt 0               # lib.c:23:11
	movl	%edi, -40(%rbp)
	.loc	1 24 14 is_stmt 1               # lib.c:24:14
	movl	-36(%rbp), %esi
	.loc	1 24 16 is_stmt 0               # lib.c:24:16
	addl	-40(%rbp), %esi
	.loc	1 24 10                         # lib.c:24:10
	movq	-32(%rbp), %rdx
	.loc	1 24 12                         # lib.c:24:12
	movl	%esi, (%rdx)
	movq	%rax, -64(%rbp)                 # 8-byte Spill
.Ltmp13:
.LBB2_3:
	.loc	1 0 12                          # lib.c:0:12
	movq	-64(%rbp), %rax                 # 8-byte Reload
	.loc	1 27 12 is_stmt 1               # lib.c:27:12
	movq	-8(%rbp), %rcx
	movl	(%rcx), %edx
	movl	%eax, %esi
	orl	%edx, %esi
	.loc	1 27 21 is_stmt 0               # lib.c:27:21
	movq	-8(%rbp), %rcx
	movl	8(%rcx), %edx
	movl	%eax, %edi
	orl	%edx, %edi
	.loc	1 27 30                         # lib.c:27:30
	movq	-32(%rbp), %rdx
	.loc	1 27 33                         # lib.c:27:33
	movl	-20(%rbp), %ecx
	movl	%edi, -68(%rbp)                 # 4-byte Spill
	.loc	1 27 5                          # lib.c:27:5
	movl	%esi, %edi
	movl	-68(%rbp), %esi                 # 4-byte Reload
	shlq	$47, %rax
	orq	%rax, %rsp
	callq	simple
.Lslh_ret_addr0:
	movq	-8(%rsp), %rax
	movq	%rsp, %rdx
	sarq	$63, %rdx
	cmpq	$.Lslh_ret_addr0, %rax
	movq	-48(%rbp), %rax                 # 8-byte Reload
	cmovneq	%rax, %rdx
.Ltmp14:
	.loc	1 29 11 is_stmt 1               # lib.c:29:11
	cmpl	$20, -20(%rbp)
	movq	%rdx, -80(%rbp)                 # 8-byte Spill
.Ltmp15:
	.loc	1 29 9 is_stmt 0                # lib.c:29:9
	jle	.LBB2_6
	jmp	.LBB2_4
.LBB2_6:
	.loc	1 0 9                           # lib.c:0:9
	movq	-80(%rbp), %rax                 # 8-byte Reload
	movq	-48(%rbp), %rcx                 # 8-byte Reload
	cmovgq	%rcx, %rax
	movq	%rax, -88(%rbp)                 # 8-byte Spill
	.loc	1 29 9                          # lib.c:29:9
	jmp	.LBB2_5
.LBB2_4:
	.loc	1 0 9                           # lib.c:0:9
	movq	-80(%rbp), %rax                 # 8-byte Reload
	movq	-48(%rbp), %rcx                 # 8-byte Reload
	cmovleq	%rcx, %rax
.Ltmp16:
	.loc	1 30 10 is_stmt 1               # lib.c:30:10
	movq	-32(%rbp), %rdx
	.loc	1 30 12 is_stmt 0               # lib.c:30:12
	movl	(%rdx), %esi
	movl	%eax, %edi
	orl	%esi, %edi
	addl	$10, %edi
	movl	%edi, (%rdx)
	movq	%rax, -88(%rbp)                 # 8-byte Spill
.Ltmp17:
.LBB2_5:
	.loc	1 0 12                          # lib.c:0:12
	movq	-88(%rbp), %rax                 # 8-byte Reload
	.loc	1 32 1 is_stmt 1                # lib.c:32:1
	shlq	$47, %rax
	orq	%rax, %rsp
	addq	$96, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Ltmp18:
.Lfunc_end1:
	.size	array, .Lfunc_end1-array
	.cfi_endproc
                                        # -- End function
	.section	.text.__llvm_retpoline_r11,"axG",@progbits,__llvm_retpoline_r11,comdat
	.hidden	__llvm_retpoline_r11            # -- Begin function __llvm_retpoline_r11
	.weak	__llvm_retpoline_r11
	.p2align	4, 0x90
	.type	__llvm_retpoline_r11,@function
__llvm_retpoline_r11:                   # @__llvm_retpoline_r11
.Lfunc_begin2:
	.cfi_startproc
# %bb.0:
	callq	.Ltmp19
.LBB1_1:                                # Block address taken
                                        # =>This Inner Loop Header: Depth=1
	pause
	lfence
	jmp	.LBB1_1
	.p2align	4, 0x90
# %bb.2:                                # Block address taken
.Ltmp19:
	movq	%r11, (%rsp)
	retq
.Lfunc_end2:
	.size	__llvm_retpoline_r11, .Lfunc_end2-__llvm_retpoline_r11
	.cfi_endproc
                                        # -- End function
	.section	.debug_abbrev,"",@progbits
	.byte	1                               # Abbreviation Code
	.byte	17                              # DW_TAG_compile_unit
	.byte	1                               # DW_CHILDREN_yes
	.byte	37                              # DW_AT_producer
	.byte	14                              # DW_FORM_strp
	.byte	19                              # DW_AT_language
	.byte	5                               # DW_FORM_data2
	.byte	3                               # DW_AT_name
	.byte	14                              # DW_FORM_strp
	.byte	16                              # DW_AT_stmt_list
	.byte	23                              # DW_FORM_sec_offset
	.byte	27                              # DW_AT_comp_dir
	.byte	14                              # DW_FORM_strp
	.byte	17                              # DW_AT_low_pc
	.byte	1                               # DW_FORM_addr
	.byte	18                              # DW_AT_high_pc
	.byte	6                               # DW_FORM_data4
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	2                               # Abbreviation Code
	.byte	46                              # DW_TAG_subprogram
	.byte	1                               # DW_CHILDREN_yes
	.byte	17                              # DW_AT_low_pc
	.byte	1                               # DW_FORM_addr
	.byte	18                              # DW_AT_high_pc
	.byte	6                               # DW_FORM_data4
	.byte	64                              # DW_AT_frame_base
	.byte	24                              # DW_FORM_exprloc
	.byte	3                               # DW_AT_name
	.byte	14                              # DW_FORM_strp
	.byte	58                              # DW_AT_decl_file
	.byte	11                              # DW_FORM_data1
	.byte	59                              # DW_AT_decl_line
	.byte	11                              # DW_FORM_data1
	.byte	39                              # DW_AT_prototyped
	.byte	25                              # DW_FORM_flag_present
	.byte	63                              # DW_AT_external
	.byte	25                              # DW_FORM_flag_present
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	3                               # Abbreviation Code
	.byte	5                               # DW_TAG_formal_parameter
	.byte	0                               # DW_CHILDREN_no
	.byte	2                               # DW_AT_location
	.byte	24                              # DW_FORM_exprloc
	.byte	3                               # DW_AT_name
	.byte	14                              # DW_FORM_strp
	.byte	58                              # DW_AT_decl_file
	.byte	11                              # DW_FORM_data1
	.byte	59                              # DW_AT_decl_line
	.byte	11                              # DW_FORM_data1
	.byte	73                              # DW_AT_type
	.byte	19                              # DW_FORM_ref4
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	4                               # Abbreviation Code
	.byte	52                              # DW_TAG_variable
	.byte	0                               # DW_CHILDREN_no
	.byte	2                               # DW_AT_location
	.byte	24                              # DW_FORM_exprloc
	.byte	3                               # DW_AT_name
	.byte	14                              # DW_FORM_strp
	.byte	58                              # DW_AT_decl_file
	.byte	11                              # DW_FORM_data1
	.byte	59                              # DW_AT_decl_line
	.byte	11                              # DW_FORM_data1
	.byte	73                              # DW_AT_type
	.byte	19                              # DW_FORM_ref4
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	5                               # Abbreviation Code
	.byte	36                              # DW_TAG_base_type
	.byte	0                               # DW_CHILDREN_no
	.byte	3                               # DW_AT_name
	.byte	14                              # DW_FORM_strp
	.byte	62                              # DW_AT_encoding
	.byte	11                              # DW_FORM_data1
	.byte	11                              # DW_AT_byte_size
	.byte	11                              # DW_FORM_data1
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	6                               # Abbreviation Code
	.byte	15                              # DW_TAG_pointer_type
	.byte	0                               # DW_CHILDREN_no
	.byte	73                              # DW_AT_type
	.byte	19                              # DW_FORM_ref4
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	0                               # EOM(3)
	.section	.debug_info,"",@progbits
.Lcu_begin0:
	.long	.Ldebug_info_end0-.Ldebug_info_start0 # Length of Unit
.Ldebug_info_start0:
	.short	4                               # DWARF version number
	.long	.debug_abbrev                   # Offset Into Abbrev. Section
	.byte	8                               # Address Size (in bytes)
	.byte	1                               # Abbrev [1] 0xb:0xf2 DW_TAG_compile_unit
	.long	.Linfo_string0                  # DW_AT_producer
	.short	12                              # DW_AT_language
	.long	.Linfo_string1                  # DW_AT_name
	.long	.Lline_table_start0             # DW_AT_stmt_list
	.long	.Linfo_string2                  # DW_AT_comp_dir
	.quad	.Lfunc_begin0                   # DW_AT_low_pc
	.long	.Lfunc_end1-.Lfunc_begin0       # DW_AT_high_pc
	.byte	2                               # Abbrev [2] 0x2a:0x5c DW_TAG_subprogram
	.quad	.Lfunc_begin0                   # DW_AT_low_pc
	.long	.Lfunc_end0-.Lfunc_begin0       # DW_AT_high_pc
	.byte	1                               # DW_AT_frame_base
	.byte	86
	.long	.Linfo_string3                  # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	1                               # DW_AT_decl_line
                                        # DW_AT_prototyped
                                        # DW_AT_external
	.byte	3                               # Abbrev [3] 0x3f:0xe DW_TAG_formal_parameter
	.byte	2                               # DW_AT_location
	.byte	145
	.byte	124
	.long	.Linfo_string5                  # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	1                               # DW_AT_decl_line
	.long	240                             # DW_AT_type
	.byte	3                               # Abbrev [3] 0x4d:0xe DW_TAG_formal_parameter
	.byte	2                               # DW_AT_location
	.byte	145
	.byte	120
	.long	.Linfo_string7                  # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	1                               # DW_AT_decl_line
	.long	240                             # DW_AT_type
	.byte	3                               # Abbrev [3] 0x5b:0xe DW_TAG_formal_parameter
	.byte	2                               # DW_AT_location
	.byte	145
	.byte	112
	.long	.Linfo_string8                  # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	1                               # DW_AT_decl_line
	.long	247                             # DW_AT_type
	.byte	3                               # Abbrev [3] 0x69:0xe DW_TAG_formal_parameter
	.byte	2                               # DW_AT_location
	.byte	145
	.byte	108
	.long	.Linfo_string9                  # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	1                               # DW_AT_decl_line
	.long	240                             # DW_AT_type
	.byte	4                               # Abbrev [4] 0x77:0xe DW_TAG_variable
	.byte	2                               # DW_AT_location
	.byte	145
	.byte	104
	.long	.Linfo_string10                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	2                               # DW_AT_decl_line
	.long	240                             # DW_AT_type
	.byte	0                               # End Of Children Mark
	.byte	2                               # Abbrev [2] 0x86:0x6a DW_TAG_subprogram
	.quad	.Lfunc_begin1                   # DW_AT_low_pc
	.long	.Lfunc_end1-.Lfunc_begin1       # DW_AT_high_pc
	.byte	1                               # DW_AT_frame_base
	.byte	86
	.long	.Linfo_string4                  # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	11                              # DW_AT_decl_line
                                        # DW_AT_prototyped
                                        # DW_AT_external
	.byte	3                               # Abbrev [3] 0x9b:0xe DW_TAG_formal_parameter
	.byte	2                               # DW_AT_location
	.byte	145
	.byte	120
	.long	.Linfo_string11                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	11                              # DW_AT_decl_line
	.long	247                             # DW_AT_type
	.byte	3                               # Abbrev [3] 0xa9:0xe DW_TAG_formal_parameter
	.byte	2                               # DW_AT_location
	.byte	145
	.byte	112
	.long	.Linfo_string12                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	11                              # DW_AT_decl_line
	.long	247                             # DW_AT_type
	.byte	3                               # Abbrev [3] 0xb7:0xe DW_TAG_formal_parameter
	.byte	2                               # DW_AT_location
	.byte	145
	.byte	108
	.long	.Linfo_string9                  # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	11                              # DW_AT_decl_line
	.long	240                             # DW_AT_type
	.byte	3                               # Abbrev [3] 0xc5:0xe DW_TAG_formal_parameter
	.byte	2                               # DW_AT_location
	.byte	145
	.byte	96
	.long	.Linfo_string8                  # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	11                              # DW_AT_decl_line
	.long	247                             # DW_AT_type
	.byte	4                               # Abbrev [4] 0xd3:0xe DW_TAG_variable
	.byte	2                               # DW_AT_location
	.byte	145
	.byte	92
	.long	.Linfo_string5                  # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	12                              # DW_AT_decl_line
	.long	240                             # DW_AT_type
	.byte	4                               # Abbrev [4] 0xe1:0xe DW_TAG_variable
	.byte	2                               # DW_AT_location
	.byte	145
	.byte	88
	.long	.Linfo_string7                  # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	12                              # DW_AT_decl_line
	.long	240                             # DW_AT_type
	.byte	0                               # End Of Children Mark
	.byte	5                               # Abbrev [5] 0xf0:0x7 DW_TAG_base_type
	.long	.Linfo_string6                  # DW_AT_name
	.byte	5                               # DW_AT_encoding
	.byte	4                               # DW_AT_byte_size
	.byte	6                               # Abbrev [6] 0xf7:0x5 DW_TAG_pointer_type
	.long	240                             # DW_AT_type
	.byte	0                               # End Of Children Mark
.Ldebug_info_end0:
	.section	.debug_str,"MS",@progbits,1
.Linfo_string0:
	.asciz	"Ubuntu clang version 11.1.0-++20211011094159+1fdec59bffc1-1~exp1~20211011214614.8" # string offset=0
.Linfo_string1:
	.asciz	"lib.c"                         # string offset=82
.Linfo_string2:
	.asciz	"/home/basavesh/research/simple_lang_sslh/LLVM" # string offset=88
.Linfo_string3:
	.asciz	"simple"                        # string offset=134
.Linfo_string4:
	.asciz	"array"                         # string offset=141
.Linfo_string5:
	.asciz	"x"                             # string offset=147
.Linfo_string6:
	.asciz	"int"                           # string offset=149
.Linfo_string7:
	.asciz	"y"                             # string offset=153
.Linfo_string8:
	.asciz	"z"                             # string offset=155
.Linfo_string9:
	.asciz	"c"                             # string offset=157
.Linfo_string10:
	.asciz	"result"                        # string offset=159
.Linfo_string11:
	.asciz	"arr1"                          # string offset=166
.Linfo_string12:
	.asciz	"arr2"                          # string offset=171
	.ident	"Ubuntu clang version 11.1.0-++20211011094159+1fdec59bffc1-1~exp1~20211011214614.8"
	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym simple
	.section	.debug_line,"",@progbits
.Lline_table_start0:
