# Speculative Load Hardening Pass
SLH is implemented as a MachineFunction Pass. It can harden a load address or a
loaded value.

## LLVM SLH pass at a high level

```C
void foo() {
  // Declare predicate state
  // FenceCallAndRet: bool - Add LFENCE at the beginning of fn
  // !FenceCallAndRet: bool -  Retreive the Caller predicate state from stack pointer

  // Nothing to do for a function which doesn't have conditional branches.

  // If a basic block starts with LFENCE, no load is vulnerable in that bb.
  // Nothing to do in a bb doesn't have any vulnerable load

  if (cond) {
    // CMOVCC: conditionally set predicate state
    ms = !cond? all_one_mask : ms;

    x = arr[e];
    x |= ms;          // Loaded value hardening

  } else {
    // CMOVCC: conditionally set predicate state
    ms = cond? all_one_mask : ms;
  }

  // store predicate at high bit of stack pointer before call
  bar();
  // retrieve predicate from stack pointer
  .
  .
  .
  .

  // add LFence for ret OR
  // Update the pred state in stack pointer
}
```
Function to start reading:
```C
bool X86SpeculativeLoadHardeningPass::runOnMachineFunction(
    MachineFunction &MF) {}
```


## When is this pass executed?
At a High-level, SLH is applied at the LLVM MIR - Machine Optimization phase and
before the Register allocation.
![Program Flow](./pics/flow.png)
![Code Generation Pipeline](./pics/code_generation_pipeline.png)
Image source:
[2017 LLVM Developers’ Meeting: M. Braun “Welcome to the back-end: The LLVM
machine representation”](https://www.youtube.com/watch?v=objxlZg01D0)

```C
// llvm/lib/Target/X86/X86TargetMachine.cpp
void X86PassConfig::addIRPasses();
bool X86PassConfig::addInstSelector();
bool X86PassConfig::addIRTranslator();
bool X86PassConfig::addLegalizeMachineIR();
bool X86PassConfig::addRegBankSelect();
bool X86PassConfig::addGlobalInstructionSelect();
bool X86PassConfig::addILPOpts();
bool X86PassConfig::addPreISel();
void X86PassConfig::addPreRegAlloc() {
  if (getOptLevel() != CodeGenOpt::None) {
    addPass(&LiveRangeShrinkID);
    addPass(createX86FixupSetCC());
    addPass(createX86OptimizeLEAs());
    addPass(createX86CallFrameOptimization());
    addPass(createX86AvoidStoreForwardingBlocks());
  }

  addPass(createX86SpeculativeLoadHardeningPass()); // SLH pass
  addPass(createX86FlagsCopyLoweringPass());
  addPass(createX86DynAllocaExpander());

  if (getOptLevel() != CodeGenOpt::None) {
    addPass(createX86PreTileConfigPass());
  }
}
void X86PassConfig::addMachineSSAOptimization(); // LVI pass is added here
void X86PassConfig::addPostRegAlloc();
void X86PassConfig::addPreSched2();
void X86PassConfig::addPreEmitPass();
void X86PassConfig::addPreEmitPass2();
bool X86PassConfig::addPostFastRegAllocRewrite();
bool X86PassConfig::addPreRewrite();
```