void simple(int x, int y, int *z, int c) {
    int result = c;
    if (x > y) {
        result += x;
    } else {
        result += y;
    }
    *z = result;
}

void array(int arr1[10], int arr2[10], int c, int *z) {
    int x, y;
    if (c > 10) {
        c = arr1[0];
        x = arr1[0];         // r = [p]
        arr1[5] = x;         // [p'] = r
        arr1[4] += 5;
        y = arr2[2];         // a = [b]
        arr2[7] = y;         // [c'] = a
        arr1[3] += 0x123;
    } else {
        x = arr1[0];
        y = arr1[x];
        *z = x + y;
    }

    simple(arr1[0], arr1[2], z, c);

    if (c > 20) {
        *z += 10;
    }
}
